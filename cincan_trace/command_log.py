from datetime import datetime
import json
import os
import pathlib
import uuid
from typing import Optional, Any, Dict

JSON_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


class CommandLogBase:
    """Command log reader/writer base class"""
    def __init__(self, log_directory: Optional[pathlib.Path] = None):
        
        self.file_name_format = '%Y-%m-%d-%H-%M-%S-%f'

        #check if .cincan contains uuid.string, don't create new folder
        if(os.path.isfile(pathlib.Path.home() / '.cincan/uid.txt')):
            with open(pathlib.Path.home() / '.cincan/uid.txt', "r") as f:
                self.directoryname =  f.read()
        else:
            #create uuid.object and folder and such
            self.directoryname = str(uuid.uuid1())

            with open(pathlib.Path.home() / '.cincan/uid.txt', "w") as uid_file:
                uid_file.write(self.directoryname)

        self.log_directory = log_directory or pathlib.Path.home() / '.cincan' / 'shared' / self.directoryname /'logs'
        self.log_directory.mkdir(parents=True, exist_ok=True)

    def write(self, json_s: Dict[str, Any]):
        log_file = self.__log_file()
        while log_file.exists():
            log_file = self.__log_file()
        with log_file.open("w") as f:
            json.dump(json_s, f)

    def __log_file(self) -> pathlib.Path:
        return self.log_directory / datetime.now().strftime(self.file_name_format)
