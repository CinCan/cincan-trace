import json
import logging
import pathlib
import re
import sys
from io import IOBase
from typing import Optional, Dict, Tuple, Set

from cincan_trace.command_log import CommandLogBase
from cincan_trace.process import TracedProcess
from cincan_trace.state import TracedState
from cincan_trace.trace_lexer import parse_call


# strace command:
# strace -xx -s 4096 -f -p <pid> -e write=0,1,2 -e read=0,1,2 -e trace=chdir,clone,close,execve,openat,read,write


class STraceReader:
    def __init__(self, log_stream: IOBase, output: Optional[IOBase] = None, command_log: Optional[CommandLogBase] = None):
        self.logger = logging.getLogger('reader')
        self.log_stream = log_stream
        self.state = TracedState()
        self.root_process: Optional[TracedProcess] = None
        self.unfinished_pattern = re.compile(r"([0-9]+)\s+([a-zA-Z0-9]+)")
        self.resume_pattern = re.compile(r"([0-9]+)\s+<\.\.\.\s+([a-zA-Z0-9]+)")
        self.unfinished: Dict[Tuple[str, str], str] = {}
        self.unknown_pids: Set[int] = set()
        self.output = output
        self.command_log = command_log

    @classmethod
    def read_file(cls, file: pathlib.Path) -> 'STraceReader':
        with file.open('r') as f:
            reader = STraceReader(f)
            reader.read_loop()
            return reader

    def read_loop(self):
        line = self.log_stream.readline()
        while line:
            line = line.strip()
            self.logger.debug(line)
            call = None
            pid = -1
            if " resumed>" in line:
                line = self.resume(line)

            if line.endswith("<unfinished ...>"):
                self.store_unfinished(line)
            else:
                pid, call = parse_call(line)
                if pid != -1 and not self.root_process:
                    self.root_process = TracedProcess(None, pid)  # first PID assumed for root

            if call:
                self.logger.debug(call)
                process = self.root_process.get_process(pid)
                if process or not call.updates_process():
                    call.update(process, self.state)
                elif pid not in self.unknown_pids:
                    self.logger.error("no such process pid=%d", pid)
                    self.unknown_pids.add(pid)

                if self.output or self.command_log:
                    for r in self.state.resolved_commands:
                        if r.output_files:
                            r_json = r.to_json()
                            if self.output:
                                self.output.write(json.dumps(r_json))
                                self.output.write('\n\n')
                                self.output.flush()
                            if self.command_log:
                                self.command_log.write(r_json)

                    self.state.resolved_commands.clear()

            line = self.log_stream.readline()
        # list unfinished stuff
        if self.state.pending_commands:
            self.logger.info("*** Incomplete commands ***")
        for cmd in self.state.pending_commands:
            self.logger.info(cmd)

    def store_unfinished(self, line: str):
        pat = self.unfinished_pattern.match(line)
        if not pat:
            return
        pid = pat.group(1)
        call = pat.group(2)
        i = line.index("<unfinished ...>")
        key = pid, call
        self.unfinished[key] = line[:i]

    def resume(self, line: str) -> Optional[str]:
        pat = self.resume_pattern.match(line)
        if not pat:
            return None
        pid = pat.group(1)
        call = pat.group(2)
        i = line.index(" resumed>")
        key = pid, call
        unfinished = self.unfinished.get(key)
        if unfinished:
            del self.unfinished[key]
            return unfinished + line[i + 9:]
