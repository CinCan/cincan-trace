import argparse
import logging
import os
import pathlib
import subprocess
import sys
from typing import Optional

from cincan_trace.command_log import CommandLogBase
from cincan_trace.configuration import Configuration
from cincan_trace.reader import STraceReader


ENVIRONMENT_VARIABLE = 'CINCAN.TRACE'

def main():
    m_parser = argparse.ArgumentParser()
    m_parser.add_argument("-l", "--log", dest="log_level", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                          help="Set the logging level", default=None)
    m_parser.add_argument('-r', '--read', nargs='?', help='Read and parse strace output from a file')
    m_parser.add_argument('-w', '--write', nargs='?', help='Write parsed JSON to a file or - for stdout')
    m_parser.add_argument('--log-to', nargs='?', help='Redirect logging to a file')
    m_parser.add_argument('command', help="the traced command and its arguments", nargs=argparse.REMAINDER)

    args = m_parser.parse_args(sys.argv[1:])
    read_file = args.read
    command = args.command
    if not command and not read_file:
        m_parser.print_help()
        sys.exit(1)

    log_file = args.log_to
    log_level = args.log_level if args.log_level else ('INFO' if log_file else 'WARNING')
    if log_level not in {'DEBUG'}:
        sys.tracebacklimit = 0

    logging.basicConfig(filename=log_file, format='%(name)s %(message)s', level=getattr(logging, log_level),
                        datefmt='%H:%M:%S')
    logger = logging.getLogger('trace')

    if args.write and args.write != '-':
        # explicitly asking to write to a file
        write_file = pathlib.Path(args.write).open('w')
    elif read_file or args.write == '-':
        # write to stdout when reading a file (or '-' given)
        write_file = sys.stdout
    else:
        write_file = None

    sub_env = os.environ.copy()
    if ENVIRONMENT_VARIABLE in sub_env:
        raise Exception(f"Suspected recursive tracing ({ENVIRONMENT_VARIABLE} environment variable is defined)")
    sub_env[ENVIRONMENT_VARIABLE] = '.'

    config = Configuration()

    command_log: Optional[CommandLogBase] = None
    if not write_file and config.is_command_log():
        command_log = CommandLogBase()
        print(f"tracing to {command_log.log_directory.as_posix()}", flush=True)
    else:
        print(f"tracing on", flush=True)

    if read_file:
        logger.debug("reading %s", read_file)
        with pathlib.Path(read_file).open('r') as st:
            reader = STraceReader(st, output=write_file, command_log=command_log)
            reader.read_loop()
    else:
        # create a named pipe for 'strace' output
        pipe = pathlib.Path(f"strace-out.{os.getpid()}")
        logger.debug("Use named pipe %s", pipe.as_posix())
        os.mkfifo(pipe)
        try:
            # start process with strace attached, writing the trace to the pipe
            parameters = ['strace', '-s', '4096', '-xx', '-f', '-e', 'write=0,1,2', '-e', 'read=0,1,2',
                          '-e', 'trace=chdir,clone,close,execve,openat,read,write',
                          '-o', pipe.as_posix()]
            parameters.extend(command)
            logger.debug(" ".join(parameters))

            with subprocess.Popen(parameters, env=sub_env) as proc:
                with pipe.open('r') as p:
                    reader = STraceReader(p, output=write_file, command_log=command_log)
                    reader.read_loop()
            if command_log:
                print(f"tracing off, log in {command_log.log_directory.as_posix()}", flush=True)
            else:
                print(f"tracing off", flush=True)
        finally:
            pipe.unlink()
