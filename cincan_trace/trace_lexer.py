import re
from typing import Optional, Tuple

from lark import Lark, Transformer, v_args, ParseError

from cincan_trace import systemcalls

action_grammar = r"""
    start: (pid call) | data_block
    call: (openat | close | chdir | clone | process_exit | execve | read | write) [tail]

    chdir: "chdir" "(" param ("," param)* ")" "=" ret_code
    clone: "clone" "(" param ("," param)* ")" "=" ret_code
    close: "close" "(" param ("," param)* ")" "=" ret_code
    execve: "execve" "(" param "," param ("," param)* ")" "=" ret_code
    openat: "openat" "(" param "," param ("," param)* ")" "=" ret_code
    read: "read" "(" param ("," param)* ")" "=" ret_code
    write: "write" "(" param ("," param)* ")" "=" ret_code
    data_block: "|" HEX_INTEGER raw_data tail
    raw_data: RAW_DATA
    process_exit: "+++"

    pid: INTEGER
    param: (array_param | hex_param | other_param)
    array_param: "[" [param] ("," param)* "]"
    hex_param: ESCAPED_STRING ["..."]
    other_param: PARAM
    ret_code: INTEGER
    
    tail: TAIL
    
    INTEGER: /\-?[0-9]+/
    HEX_INTEGER: /\-?[0-9a-fA-F]+/
    RAW_DATA: /([0-9a-fA-F ][0-9a-fA-F ] ){8} ([0-9a-fA-F ][0-9a-fA-F ] ){8}/
    PARAM: /[^][)(," \t][^][)(,"]*/
    TAIL.10: /[^\r\n]+/
    %import common.ESCAPED_STRING -> ESCAPED_STRING

    WS.1: /[ \t]+/
    %ignore WS
"""


@v_args(inline=True)  # Affects the signatures of the methods
class CommandTransformer(Transformer):
    def __init__(self):
        super().__init__()

    def start(self, *args):
        if len(args) == 1:
            return -1, args[0]
        else:
            return args  # pid, call

    def call(self, *args):
        return args[0]

    def chdir(self, dir_name, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.ChdirCall(dir_name.decode('ascii'))

    def clone(self, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.CloneCall(ret_code)

    def close(self, fd, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.CloseCall(int(fd))

    def execve(self, command, arguments, *params):
        args = [a.decode('ascii') for a in arguments]
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.ExecveCall(command=command.decode('ascii'), arguments=args)

    def read(self, fd, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.ReadCall(int(fd))

    def write(self, fd, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.WriteCall(int(fd))

    def openat(self, param1, file_name, *params):
        ret_code = params[-1]
        if ret_code < 0:
            return None
        return systemcalls.OpenAtCall(file_name.decode('ascii'), ret_code)

    def process_exit(self):
        return systemcalls.ProcessExit()

    def pid(self, token):
        return int(token.value)

    def param(self, value):
        return value

    def array_param(self, *items):
        return list(items)

    def hex_param(self, token):
        hex_strings = token.value[1:-1].split('\\x')
        value = bytearray()
        for h in hex_strings:
            if h:
                value.append(int(h, 16))
        return value

    def other_param(self, token):
        return token.value

    def ret_code(self, token):
        return int(token.value)

    def data_block(self, offset, value, tail):
        return systemcalls.DataBlock(value)

    def raw_data(self, hex_data):
        value = bytes.fromhex(hex_data.value.strip())
        return value


CALL_PARSER = Lark(action_grammar, parser='lalr', transformer=CommandTransformer())

PRE_PATTERN = re.compile(r"([0-9]+)\s+([a-zA-Z0-9]+)\(([^,])")


def parse_call(text: str) -> Tuple[int, Optional[systemcalls.SystemCall]]:

    # NOTE: There may be a LOT of large read/writes, but we are only intested on the FD
    # which is read/written, not at all about the data
    prefix = PRE_PATTERN.match(text)
    if prefix:
        pid = int(prefix.group(1))
        cmd = prefix.group(2)
        p1 = prefix.group(3)
        if cmd == 'read':
            return pid, systemcalls.ReadCall(int(p1))
        if cmd == 'write':
            return pid, systemcalls.WriteCall(int(p1))

    try:
        pid, cmd = CALL_PARSER.parse(text)
        return pid, cmd
    except ParseError as e:
        return -1, None
