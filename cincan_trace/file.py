import hashlib
import pathlib
from typing import Set, Callable, Optional


class TracedFile:
    def __init__(self, name: str, path: pathlib.Path):
        self.pids: Set[int] = set()
        self.read = False
        self.written = False
        self.name = name
        self.path = path

        self.hash = None  # later hashlib.sha256()
        self.digest: Optional[str] = None
        self.data_length = 0

    def is_digest_ready(self) -> bool:
        return self.digest is not None

    def is_digest_pending(self) -> bool:
        return self.hash is not None

    def update_data(self, data: bytes = None) -> 'TracedFile':
        if self.hash is None:
            self.hash = hashlib.sha256()
        if data:
            self.data_length += len(data)
            self.hash.update(data)
        return self

    def ready_digest(self) -> 'TracedFile':
        if self.hash is not None:
            self.digest = self.hash.hexdigest()
            self.hash = None
        return self

    def copy_if_open(self) -> 'TracedFile':
        if self.digest:
            return self  # closed, no changes to be expected
        nf = TracedFile(self.name, self.path)
        nf.read = self.read
        nf.written = self.written
        if self.hash:
            nf.hash = self.hash.copy()
        nf.data_length = self.data_length
        return nf

    def __str__(self):
        read = " read" if self.read else ""
        written = " written" if self.written else ""
        len = f" len={self.data_length}" if self.data_length else ""
        digest = f" sha256={self.digest}" if self.digest else ''

        return f"{self.path.as_posix()} {read}{written}{len}{digest}"


class FileDigestManager:
    def submit(self, file: TracedFile, ready: Callable[[TracedFile, Optional[Exception]], None]):
        # FIXME: Without threading now
        try:
            if file.path.is_dir():
                file.digest = '-'
                ready(file, None)
                return
            with file.path.open('rb') as f:
                while True:
                    buf = f.read(4096)
                    if not buf:
                        break
                    file.update_data(buf)
            ready(file, None)
        except Exception as e:
            file.digest = '-'
            ready(file, e)
        finally:
            file.ready_digest()
