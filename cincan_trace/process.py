import logging
import pathlib
from typing import Dict, Optional

from cincan_trace.command import SyntheticCommand
from cincan_trace.file import TracedFile


class TracedProcess:
    def __init__(self, parent: Optional['TracedProcess'], pid: int):
        self.logger = logging.getLogger(f"[{pid}]")
        self.parent = parent
        self.pid = pid
        self.sub_process: Dict[int, 'TracedProcess'] = {}
        self.open_files: Dict[int, TracedFile] = {}
        self.all_files: Dict[pathlib.Path, TracedFile] = {}
        self.work_dir = pathlib.Path('/')
        self.command: Optional[SyntheticCommand] = None

    def get_process(self, pid: int) -> Optional['TracedProcess']:
        if pid == self.pid:
            return self
        return self.sub_process.get(pid)

    def create_sub_process(self, pid: int) -> 'TracedProcess':
        np = TracedProcess(self, pid)
        np.work_dir = self.work_dir

        # NOTE: No we do *not* copy any files over - will change in future?

        # Keep command, this is sub-process for the command
        if self.command:
            self.command.pids.add(pid)  # one more process filling in data for the command
            np.command = self.command

        self.logger.info("new process pid=%d", pid)
        p = self
        while p:
            p.sub_process[pid] = np
            p = p.parent
        return np

    def terminate(self):
        self.logger.info("terminate")
        for f in self.open_files.values():
            self.__close_file(f)
        self.open_files.clear()
        if self.parent:
            del self.parent.sub_process[self.pid]

    def open_file(self, fd: int, name: str) -> TracedFile:
        file_name = pathlib.Path(name)
        if file_name.is_absolute():
            path = file_name
        else:
            path = (self.work_dir / file_name).resolve()
        path = path.resolve()

        if path in self.all_files:
            file = self.all_files[path]
        else:
            file = TracedFile(name, path)
            self.all_files[path] = file
        file.pids.add(self.pid)

        self.open_files[fd] = file
        self.logger.info("open file fd=%d %s", fd, path.as_posix())
        return file

    def file_read(self, fd: int) -> Optional[TracedFile]:
        file = self.open_files.get(fd)
        if not file and fd == 0:
            file = self.open_file(fd, '/stdin').update_data()  # digest calculated on-the-fly
        if file and not file.written:  # if we have written the file, it is not input
            file.read = True
        return file

    def file_write(self, fd: int) -> Optional[TracedFile]:
        file = self.open_files.get(fd)
        if not file and 0 < fd < 3:
            file = self.open_file(fd, '/stdout' if fd == 1 else '/stderr').update_data()  # digest calculated on-the-fly
        if file:
            file.written = True
        return file

    def close_file(self, fd: int):
        file = self.open_files.get(fd)
        if file:
            del self.open_files[fd]
            self.__close_file(file)

    def __close_file(self, file: Optional[TracedFile]) -> Optional[TracedFile]:
        if file:
            if file.is_digest_pending():
                # if digest is being calculated on-the-fly (stdxxx), then we are now ready with it
                file.ready_digest()
            self.logger.debug("close file %s", file)
        return file

    def chdir(self, dir_name: str):
        new_path = pathlib.Path(dir_name)
        if new_path.is_absolute():
            self.work_dir = new_path
        else:
            self.work_dir = self.work_dir / dir_name
        self.logger.info("chdir %s", self.work_dir)

    def execute_new(self, command: SyntheticCommand):
        self.logger.info("new command: %s %s", command.command, " ".join(command.arguments))
        self.command = command

        # reset all files
        self.all_files.clear()
        for f in self.open_files.values():
            self.__close_file(f)
        self.open_files.clear()

    def update_command(self):
        in_files = [f for f in self.all_files.values() if f.read]
        out_files = [f for f in self.all_files.values() if f.written]
        self.command.update_files(in_files, out_files)


    def __str__(self):
        cmd = ""
        if self.command:
            cmd = f" {self.command.command}"
        return f"pid={self.pid}{cmd}"
