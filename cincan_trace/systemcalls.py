from typing import List

from cincan_trace.process import TracedProcess
from cincan_trace.state import TracedState


class SystemCall:
    def updates_process(self) -> bool:
        return True

    def update(self, process: TracedProcess, state: TracedState):
        pass


class ChdirCall(SystemCall):
    def __init__(self, dir_name: str):
        self.dir_name = dir_name

    def update(self, process: TracedProcess, state: TracedState):
        process.chdir(self.dir_name)

    def __repr__(self):
        return f"chdir({self.dir_name})"


class CloneCall(SystemCall):
    def __init__(self, new_pid: int):
        self.new_pid = new_pid

    def update(self, process: TracedProcess, state: TracedState):
        process.create_sub_process(self.new_pid)

    def __repr__(self):
        return f"clone() = {self.new_pid}"


class OpenAtCall(SystemCall):
    def __init__(self, file_name: str, fd: int):
        self.file_name = file_name
        self.fd = fd

    def update(self, process: TracedProcess, state: TracedState):
        include = True
        for inc, path in state.file_filter:
            if self.file_name.startswith(path):
                include = inc
        if include and self.fd >= 0:
            process.open_file(self.fd, self.file_name)

    def __repr__(self):
        return f"openat({self.file_name}) = {self.fd}"


class CloseCall(SystemCall):
    def __init__(self, fd: int):
        self.fd = fd

    def update(self, process: TracedProcess, state: TracedState):
        if self.fd >= 0:
            process.close_file(self.fd)

    def __repr__(self):
        return f"close({self.fd})"


class ExecveCall(SystemCall):
    def __init__(self, command: str, arguments: List[str]):
        self.command = command
        self.arguments = arguments

    def update(self, process: TracedProcess, state: TracedState):
        state.new_command(process, self.command, self.arguments)

    def __repr__(self):
        args = ", ".join(self.arguments)
        return f"execve({self.command}, [{args}])"


class ReadCall(SystemCall):
    def __init__(self, fd: int):
        self.fd = fd

    def update(self, process: TracedProcess, state: TracedState):
        file = process.file_read(self.fd)
        state.recent_file = file

    def __repr__(self):
        return f"read({self.fd})"


class WriteCall(SystemCall):
    def __init__(self, fd: int):
        self.fd = fd

    def update(self, process: TracedProcess, state: TracedState):
        file = process.file_write(self.fd)
        state.recent_file = file

    def __repr__(self):
        return f"write({self.fd})"


class DataBlock(SystemCall):
    def __init__(self, data: bytes):
        self.data = data

    def __repr__(self):
        return '| ' + self.data.hex()

    def updates_process(self) -> bool:
        return False

    def update(self, process: TracedProcess, state: TracedState):
        if state.recent_file:
            file = state.recent_file
            if file.hash:
                file.update_data(self.data)
            #  state.logger.info("%d/%d bytes to file %s", len(self.data), file.data_length, file.path.as_posix())


class ProcessExit(SystemCall):
    def __repr__(self):
        return f"+++ exit"

    def update(self, process: TracedProcess, state: TracedState):
        process.terminate()
        state.detach_command(process)
