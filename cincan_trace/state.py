from datetime import datetime
import itertools
import logging
import pathlib
from typing import Optional, List, Tuple

from cincan_trace.command import SyntheticCommand
from cincan_trace.file import TracedFile, FileDigestManager
from cincan_trace.process import TracedProcess


class TracedState:
    def __init__(self):
        self.logger = logging.getLogger('state')
        self.pending_commands: List[SyntheticCommand] = []
        self.resolved_commands: List[SyntheticCommand] = []
        self.file_manager = FileDigestManager()
        self.recent_file: Optional[TracedFile] = None

        home_dir = pathlib.Path.home()
        self.file_filter: List[Tuple[bool, str]] = [
            (False, '/dev/'),  # reading e.g. from dev/urandom causes infinite digest calculation
            (False, '/usr/'),
            (False, '/etc/'),
            (False, '/lib/'),
            (False, '/proc/'),
            (False, '/sys/'),
            (False, '/bin/'),
            (False, '/sbin/'),
            (False, f'{home_dir}/.')
        ]

    def new_command(self, process: TracedProcess, command: str, arguments: List[str]) -> SyntheticCommand:
        if process.command:
            self.detach_command(process)
        cmd = SyntheticCommand(process.pid, command, arguments)
        process.execute_new(cmd)
        return cmd

    def detach_command(self, process: TracedProcess) -> Optional[SyntheticCommand]:
        cmd = process.command
        if cmd:
            cmd.pids.discard(process.pid)
            process.update_command()
            process.command = None
            if not cmd.pids:
                # all processes of the command have terminated
                self.__command_now_pending(cmd, process)
        return cmd

    def __command_now_pending(self, command: SyntheticCommand, process: TracedProcess):
        command.end_time = datetime.now()
        for f in itertools.chain(command.input_files.values(), command.output_files.values()):
            if not f.is_digest_ready() and not f.is_digest_pending():
                self.file_manager.submit(f, self.file_digest_ready)  # must calculate digest for this file

        self.pending_commands.append(command)
        self.__check_resolved_commands()

    def __check_resolved_commands(self):
        while True:
            for cmd in self.pending_commands:
                if cmd.is_ready():
                    break
            else:
                break
            self.logger.info(cmd)
            self.pending_commands.remove(cmd)
            self.resolved_commands.append(cmd)

    def file_digest_ready(self, file: TracedFile, error: Optional[Exception]):
        if error:
            self.logger.error(error)
        self.__check_resolved_commands()
