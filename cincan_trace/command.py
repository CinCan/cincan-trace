import pathlib
from datetime import datetime
from typing import List, Set, Dict, Tuple, Any, Optional

from cincan_trace.command_log import JSON_TIME_FORMAT
from cincan_trace.file import TracedFile


class SyntheticCommand:
    def __init__(self, pid: int, command: str, arguments: List[str]):
        self.pid = pid
        self.pids: Set[int] = {pid}
        self.command = command
        self.arguments = arguments
        self.start_time: datetime = datetime.now()
        self.end_time: Optional[datetime] = None
        self.input_files: Dict[pathlib.Path, TracedFile] = {}
        self.output_files: Dict[pathlib.Path, TracedFile] = {}

    def list_input_files(self) -> List[Tuple[str, str]]:
        return [(f.path.as_posix(), f.digest[:16]) for f in sorted(self.input_files.values(), key=lambda f: f.path)]

    def list_output_files(self) -> List[Tuple[str, str]]:
        return [(f.path.as_posix(), f.digest[:16]) for f in sorted(self.output_files.values(), key=lambda f: f.path)]

    def update_files(self, input_files: List[TracedFile], output_files: List[TracedFile]):
        for f in input_files:
            self.input_files[f.path] = f
        for f in output_files:
            self.output_files[f.path] = f

    def is_ready(self) -> bool:
        if self.pids:
            return False  # some processing are still running
        for f in self.input_files.values():
            if not f.is_digest_ready():
                return False
        for f in self.output_files.values():
            if not f.is_digest_ready():
                return False
        return True

    def to_json(self) -> Dict[str, Any]:
        m = {'command': self.arguments, 'executable': self.command}
        files = []
        for f in self.input_files.values():
            f_posix = f.path.as_posix()
            d = {'name': f.name}
            if f.name != f_posix:
                d['path'] = f_posix
            if f.digest and f.digest != '-':
                d['sha256'] = f.digest
            files.append(d)
        m['start_time'] = self.start_time.strftime(JSON_TIME_FORMAT)
        m['end_time'] = self.end_time.strftime(JSON_TIME_FORMAT) if self.end_time else None
        m['input'] = files
        files = []
        for f in self.output_files.values():
            f_posix = f.path.as_posix()
            d = {'name': f.name}
            if f.name != f_posix:
                d['path'] = f_posix
            if f.digest and f.digest != '-':
                d['sha256'] = f.digest
            files.append(d)
        m['output'] = files
        return m

    def __repr__(self):
        args = ", ".join(self.arguments)
        in_files = ''
        if self.input_files:
            in_files = "\n in  " + "\n in  ".join([f.__str__() for f in self.input_files.values()])
        out_files = ''
        if self.output_files:
            out_files = "\n out " + "\n out ".join([f.__str__() for f in self.output_files.values()])
        return f"execve({self.command}, [{args}]) pid={self.pid}{in_files}{out_files}"
