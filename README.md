# Cincan trace

:warning: Work in progress... currently best to go away and come back later!

Command `cincan-trace` can be used to create a trace of executed
commands and the files manipulated by the commands.
This trace is intended to show how a particular file has been
created, when it was created and by how (if shared in a team).

Cincan trace is produced by tracking executed processes and
the hash codes of the files processes have read and written,
Process forks and 'execve's are also traced and mapped to file
accesses.

The command 'strace' is used internally to produce the raw trace.
This command must be installed.

## Installation

Cincan trace is currently only available through *gitlab*, install it like this:

    % sudo pip3 install git+https://gitlab.com/cincan/cincan-trace

## Usage

To collect the log, you must enable "cincan logging". 
This is done identically as for the command 'cincan',
see [Cincan command logging](https://gitlab.com/CinCan/cincan-command/blob/master/logging.md).

After that, you can simply prefix commands you wish to log
with 'cincan-trace':

    $ cincan-trace <command>
    tracing on
    ...
    tracing off

Where `...` stands for the output from the command. 

The created trace is written as JSON files into a logging directory.
The log is under `~/.cincan/shared/<uid>/logs` directory, see
[Cincan command logging](https://gitlab.com/CinCan/cincan-command/blob/master/logging.md)
for more details.

You may want to capture a whole session with bash rather
than individual commands. This can be achieved simply by tracing
the interactive session with the 'bash' command:

    $ cincan-trace bash
    tracing on
    ...

FIXME: What to do with the log
