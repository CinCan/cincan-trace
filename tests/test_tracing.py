import pathlib

from cincan_trace.reader import STraceReader

TRACE_FILES = pathlib.Path('tests/sample-traces')


def test_unzip_trace():
    r = STraceReader.read_file(TRACE_FILES / "unzip.strace")

    assert len(r.state.resolved_commands) == 1
    cmd = r.state.resolved_commands[0]
    assert cmd.pid == 18693
    assert cmd.command == '/usr/bin/unzip'
    assert cmd.arguments == ['unzip', 'source/sample.zip']
    assert cmd.list_input_files() == [
        ('/source/sample.zip', '-')
    ]
    assert cmd.list_output_files() == [
        ('/Title.docx', '-'),
        ('/sample.pdf', '-'),
        ('/stdout', 'bc5d730f4f4b3997'),
        ('/text_txt.pdf', '-'),
    ]

    assert len(r.state.pending_commands) == 0
    assert not r.unknown_pids


def test_minion_unzip_trace():
    r = STraceReader.read_file(TRACE_FILES / "minion-unzip.strace")
    assert len(r.state.resolved_commands) == 15

    cmd = r.state.resolved_commands[0]
    assert cmd.pid == 21405
    assert cmd.command == '/usr/bin/file'
    assert cmd.arguments == ['file', '-b', '--mime-type', 'source/sample.zip']
    assert cmd.list_input_files() == [
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip', '03174b9c86941a74')
    ]
    assert cmd.list_output_files() == [
        ('/stdout', '842ae46c896538b8')
    ]

    cmd = r.state.resolved_commands[1]
    assert cmd.pid == 21404
    assert cmd.command == '/bin/sh'
    assert cmd.arguments == ['/bin/sh', '-c', 'file -b --mime-type source/sample.zip']
    assert cmd.list_input_files() == []
    assert cmd.list_output_files() == []

    cmd = r.state.resolved_commands[2]
    assert cmd.pid == 21410
    assert cmd.command == '/usr/bin/unzip'
    assert cmd.arguments == ['unzip', 'source/sample.zip', '-d', 'source/sample.zip.d/unzip']
    assert cmd.list_input_files() == [
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip', '03174b9c86941a74')
    ]
    assert cmd.list_output_files() == [
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip.d/unzip/Title.docx', 'a7c1e10e8b12c62f'),
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip.d/unzip/sample.pdf', '67025c35e17abdfc'),
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip.d/unzip/text_txt.pdf', '3df3d9dcc50fc4f0'),
        ('/stdout', '0c5dd9ef07bd3101'),
    ]

    cmd = r.state.resolved_commands[3]
    assert cmd.pid == 21409
    assert cmd.command == '/bin/sh'
    assert cmd.arguments == ['/bin/sh', '-c', 'unzip source/sample.zip -d source/sample.zip.d/unzip']
    assert cmd.list_input_files() == []
    assert cmd.list_output_files() == []

    cmd = r.state.resolved_commands[4]
    assert cmd.pid == 21417
    assert cmd.command == '/usr/bin/file'
    assert cmd.arguments == ['file', '-b', '--mime-type', 'source/sample.zip.d/unzip/sample.pdf']
    assert cmd.list_input_files() == [
        ('/home/rauli/cincan/demo/pdf-pipeline/source/sample.zip.d/unzip/sample.pdf', '67025c35e17abdfc'),
    ]
    assert cmd.list_output_files() == [
        ('/stdout', '21347cc8b7139278'),
    ]

    cmd = r.state.resolved_commands[5]
    assert cmd.pid == 21415

    cmd = r.state.resolved_commands[12]
    assert cmd.pid == 21402

    #assert not r.unknown_pids
