import re

from cincan_trace import systemcalls
from cincan_trace.trace_lexer import parse_call


def test_chdir():
    pid, call = parse_call(r"""4969  chdir("\x74\x65\x73\x74\x69")     = 0 ENOENT (No such file or directory)""")
    assert pid == 4969
    assert isinstance(call, systemcalls.ChdirCall)
    assert call.dir_name == 'testi'


def test_clone():
    _, call = parse_call(r"""6139  clone(child_stack=0x7fe128098fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe1280999d0, tls=0x7fe128099700, child_tidptr=0x7fe1280999d0) = 6140""")
    assert isinstance(call, systemcalls.CloneCall)
    assert call.new_pid == 6140

    _, call = parse_call(r"""969  clone(child_stack=NULL, flags=C|SIGCHLD, child_tidptr=0x7f162ea52a10) = 502""")
    assert isinstance(call, systemcalls.CloneCall)
    assert call.new_pid == 502

def test_close():
    _, call = parse_call(r"""4969  close(3)   = 0""")
    assert isinstance(call, systemcalls.CloseCall)
    assert call.fd == 3


def test_execve():
    _, call = parse_call(r"""5082  execve("\x2f\x75\x73\x72\x2f\x75\x6d", ["\x6d\x64\x35\x73\x75\x6d", "\x6f\x75\x74"], 0x55745e8260a0 /* 62 vars */) = 0""")
    assert isinstance(call, systemcalls.ExecveCall)
    assert call.command == '/usr/um'
    assert call.arguments == ['md5sum', 'out']


def test_openat():
    _, call = parse_call(r"""4969  openat(AT_FDCWD, "\x2f\x2e\x6d\x6f", O_RDONLY) = 72 ENOENT (No such ...)""")
    assert isinstance(call, systemcalls.OpenAtCall)
    assert call.file_name == '/.mo'
    assert call.fd == 72


def test_read():
    _, call = parse_call(r"""4969  read(0, "\x12", 1)                = 1""")
    assert isinstance(call, systemcalls.ReadCall)
    assert call.fd == 0


def test_write():
    _, call = parse_call(r"""5082  write(1, "\x37\x36\x31\x34\x31\x62"..., 42) = 42""")
    assert isinstance(call, systemcalls.WriteCall)
    assert call.fd == 1


def test_process_exit():
    pid, call = parse_call(r"""5026  +++ exited with 0 +++""")
    assert pid == 5026
    assert isinstance(call, systemcalls.ProcessExit)


def test_raw_data():
    _, call = parse_call(r""" | 009a0  32 20 30 36 32 36 20 66  30 32 39 20 33 38 65 63  2 0626 f029 38ec |""")
    assert call.data == b'2 0626 f029 38ec'

    _, call = parse_call(r""" | 00000  37 62 31 63 34 64 33 36  30 61 30 62 32 33 63 39  7b1c4d360a0b23c9 |""")
    assert isinstance(call, systemcalls.DataBlock)
    assert call.data == b"7b1c4d360a0b23c9"

    _, call = parse_call(r""" | 00020  20 20 6f 75 74 2e 62 69  6e 0a                      out.bin.       |""")
    assert call.data == b"  out.bin\n"


def test_regexp():
    pat = re.compile('[^",]+')
    m = pat.match('abba')
    assert m
    m = pat.match('"abba"')
    assert not m
    m = pat.match('ab,ba')
    assert m
